module Game.Implvz.Attributes.Position (
    Position (..)
  , PositionA (..)
  , interpolate
) where

import Game.Implvz.Types

data Position = Position {
    positionX :: !Float
  , positionY :: !Float
  } deriving (Eq, Show)

class PositionA a where
    getPosition :: a -> Position
    setPosition :: Position -> a -> a

instance Interpolate Position where
    interpolate scalar old new =
        Position {
            positionX = positionX old * (1 - scalar) + positionX new * scalar
          , positionY = positionY old * (1 - scalar) + positionY new * scalar
          }
