module Game.Implvz.Attributes.Dimension (
    Dimension (..)
  , DimensionA (..)
) where

data Dimension = Dimension {
    dimensionWidth  :: !Float
  , dimensionHeight :: !Float
  } deriving (Eq, Show)

class DimensionA a where
    getDimension :: a -> Dimension
    setDimension :: Dimension -> a -> a
