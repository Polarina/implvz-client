module Game.Implvz.Attributes.Velocity (
    Velocity (..)
  , VelocityA (..)
) where

data Velocity = Velocity {
    velocitySpeed    :: !Float
  , velocityRotation :: !Float -- ^ in radians
  } deriving (Eq, Show)

class VelocityA a where
    getVelocity :: a -> Velocity
    setVelocity :: Velocity -> a -> a

instance Num Velocity where
    (Velocity a b) + (Velocity c d) = Velocity (a + c) (b + d)
    (Velocity a b) - (Velocity c d) = Velocity (a - c) (b - d)
    (Velocity a b) * (Velocity c d) = Velocity (a * c) (b * d)
    negate (Velocity a b)           = Velocity (negate a) (negate b)
    abs (Velocity a b)              = Velocity (abs a) (abs b)
    signum (Velocity a b)           = Velocity (signum a) (signum b)
    fromInteger n                   = let n' = fromInteger n in Velocity n' n'
