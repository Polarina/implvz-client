module Game.Implvz.Attributes.Rotation (
    Rotation (..)
  , RotationA (..)
) where

import Game.Implvz.Types

data Rotation = Rotation {
    rotationAngle :: !Float -- ^ in radians
  } deriving (Eq, Show)

class RotationA a where
    getRotation :: a -> Rotation
    setRotation :: Rotation -> a -> a

instance Interpolate Rotation where
    interpolate scalar old new =
        Rotation {
            rotationAngle = rotationAngle old * (1 - scalar) + rotationAngle new * scalar
          }

instance Num Rotation where
    (Rotation a) + (Rotation b) = Rotation $ a + b
    (Rotation a) - (Rotation b) = Rotation $ a - b
    (Rotation a) * (Rotation b) = Rotation $ a * b
    negate (Rotation a)         = Rotation $ negate a
    abs (Rotation a)            = Rotation $ abs a
    signum (Rotation a)         = Rotation $ signum a
    fromInteger n               = Rotation $ fromInteger n
