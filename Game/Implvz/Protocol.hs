module Game.Implvz.Protocol (
    ClientMessage (..)
  , ServerMessage (..)
  , encodeC
  , decodeS
) where

import Codec.Picture
import Data.Attoparsec.ByteString
import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy as L
import qualified Data.Map as M
import qualified Data.Vector.Storable as V
import Game.Implvz.Attributes
import Game.Implvz.Bencode
import Game.Implvz.Types
import Unsafe.Coerce

instance Eq (Image a) where
    a == b = imageWidth  a == imageWidth  b &&
             imageHeight a == imageHeight b &&
             V.unsafeToForeignPtr0 (unsafeCoerce (imageData a) :: V.Vector Int) ==
             V.unsafeToForeignPtr0 (unsafeCoerce (imageData b) :: V.Vector Int)

instance Show (Image a) where
    show = const "<image>"

data ClientMessage = CPing
                   | CServerList
                   | CLogin {
                     cloginName :: !PlayerName
                   }
                   | CMove {
                     cmovePosition :: !Position
                   , cmoveRotation :: !Rotation
                   }
                   | CChatMessage {
                     cchatMessage :: !S.ByteString
                   }
                   | CWeapon {
                     cweapon :: !Int
                   }
                   | CFire
                   deriving (Eq, Show)

data ServerMessage = SServerList {
                     scurrentPlayers :: !Int
                   , smaximumPlayers :: !Int
                   }
                   | SMove {
                     smoveEntity   :: !EntityID
                   , smovePosition :: !Position
                   , smoveRotation :: !Rotation
                   }
                   | SChatMessage {
                     schatPlayer  :: !PlayerName
                   , schatMessage :: !S.ByteString
                   }
                   | SKick {
                     skickReason :: !S.ByteString
                   }
                   | SClientConnected {
                     splayers    :: ![(EntityID, PlayerName)]
                   }
                   | SMap {
                     smapBitmap :: !(Image PixelRGBA8)
                   }
                   | SHealth {
                     shealths    :: ![(PlayerName, Float)]
                   }
                   | SUnregisterEntity {
                     sentity :: !EntityID
                   }
                   | SScore {
                     sscores :: ![(PlayerName, Integer)]
                   }
                   deriving (Eq, Show)

encodeC :: ClientMessage -> S.ByteString
encodeC msg = S.concat . L.toChunks . encode . BDictionary . M.fromAscList $ case msg of
    CPing             -> []

    CServerList       -> [("",        BInteger 0)]

    CLogin name       -> [("",        BInteger 1)
                         ,("name",    BString name)]

    CMove pos rot     -> [("",        BInteger 3)
                         ,("rot",     bfloat $ rotationAngle rot)
                         ,("x",       bfloat $ positionX pos)
                         ,("y",       bfloat $ positionY pos)]

    CChatMessage chat -> [("",        BInteger 4)
                         ,("message", BString chat)]

    CWeapon weapon    -> [("",        BInteger 6)
                         ,("weapon",  BInteger $ fromIntegral weapon)]

    CFire             -> [("",        BInteger 7)]
    where
    bfloat :: Float -> BMessage
    bfloat n = BInteger . floor $ n * 1000000

decodeS :: Parser ServerMessage
decodeS = decode >>= \msg -> return $ case msg of
    BDictionary msg' -> let (Just (BInteger msgid)) = M.lookup "" msg' in case msgid of
        0x00 -> let scurrentPlayers = bint $ msg' M.! "current"
                    smaximumPlayers = bint $ msg' M.! "max"
                in SServerList {..}

        0x04 -> let positionX     = bfloat $ msg' M.! "x"
                    positionY     = bfloat $ msg' M.! "y"
                    rotationAngle = bfloat $ msg' M.! "rot"
                    smoveEntity   = bint $ msg' M.! "entity"
                    smovePosition = Position{..}
                    smoveRotation = Rotation{..}
                in SMove {..}

        0x06 -> let schatPlayer = bstr $ msg' M.! "player"
                    schatMessage = bstr $ msg' M.! "message"
                in SChatMessage {..}

        0x07 -> let skickReason = bstr $ msg' M.! "reason"
                in SKick {..}

        0x09 -> let ids      = map bint . blist $ msg' M.! "ids"
                    names    = map bstr . blist $ msg' M.! "names"
                    splayers = zip ids names
                in SClientConnected {..}

        0x0A -> let img' = decodePng . bstr $ msg' M.! "map"
                    (Right img) = img'
                    (ImageRGBA8 smapBitmap) = img
                in case img' of
                    Left err -> error err
                    _ -> SMap {..}

        0x0B -> let players = map bstr . blist $ msg' M.! "entity"
                    healths = map bfloat . blist $ msg' M.! "health"
                    shealths = zip players healths
                in SHealth {..}

        0x0C -> let sentity = bint $ msg' M.! "entity"
                in SUnregisterEntity {..}

        0x0D -> let players = map bstr . blist $ msg' M.! "players"
                    scores  = map bint . blist $ msg' M.! "scores"
                    sscores = zip players scores
                in SScore {..}

        _ -> error $ "unknown server message id " ++ show msgid
    _ -> error "not a dictionary"
    where
    bfloat :: (Fractional a) => BMessage -> a
    bfloat (BInteger a) = fromInteger a / 1000000
    bfloat _ = error "not a floating-point"

    bint :: (Num a) => BMessage -> a
    bint (BInteger a) = fromInteger a
    bint _ = error "not an integer"

    blist :: BMessage -> [BMessage]
    blist (BList a) = a
    blist _ = error "not a list"

    bstr :: BMessage -> S.ByteString
    bstr (BString a) = a
    bstr _ = error "not a string"
