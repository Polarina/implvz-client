module Game.Implvz.Bencode (
    BMessage (..)
  , decode
  , encode
) where

import Blaze.ByteString.Builder as B
import Blaze.ByteString.Builder.ByteString as BS
import qualified Blaze.ByteString.Builder.Char8 as B8
import Control.Applicative
import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy as L
import Data.List (foldl')
import qualified Data.Map as M
import Data.Monoid
import Data.String
import Prelude hiding (take)

data BMessage = BDictionary (M.Map S.ByteString BMessage)
              | BInteger Integer
              | BList [BMessage]
              | BString S.ByteString
              deriving (Eq, Show)

instance IsString BMessage where
    fromString = BString . fromString

decode :: Parser BMessage
decode = element
    where
    element = dictionary <|> integer <|> list <|> bytestring

    dictionary  = do
        kv <- "d" *> many ((,) <$> bytestring' <*> element) <* "e"
        return $! BDictionary $ M.fromList kv
    integer     = BInteger <$> ("i" *> signed decimal <* "e")
    list        = BList    <$> ("l" *> many element <* "e")
    bytestring  = BString  <$> bytestring'
    bytestring' = decimal <* ":" >>= take

encode :: BMessage -> L.ByteString
encode = toLazyByteString . enc
    where
    enc (BDictionary dict) = foldl' (\s (k, v) -> s <> encString k <> enc v) (B8.fromChar 'd') (M.toAscList dict) <> B8.fromChar 'e'
    enc (BInteger int)    = B8.fromChar 'i' <> B8.fromString (show int) <> B8.fromChar 'e'
    enc (BList msg)       = B8.fromChar 'l' <> mconcat (map enc msg) <> B8.fromChar 'e'
    enc (BString str)     = encString str

    encString str = B8.fromString (show $ S.length str) <> B8.fromChar ':' <> BS.fromByteString str
