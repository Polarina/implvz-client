module Game.Implvz.Components.Movement (
    MovementC (..)
  , calculateVelocity
) where

import Game.Implvz.Attributes
import Game.Implvz.Types

class (PositionA a, RotationA a, VelocityA a) => MovementC a where
    move' :: Time -> a -> a
    move' t a =
        setPosition Position {
            positionX = positionX + velocitySpeed * cos r * t
          , positionY = positionY + velocitySpeed * sin r * t
          } .
        setRotation Rotation {
            rotationAngle = r
          } $ a
        where
        Position{..} = getPosition a
        Rotation{..} = getRotation a
        Velocity{..} = getVelocity a
        r = rotationAngle + velocityRotation * t

calculateVelocity :: Position -- ^ old position
                  -> Position -- ^ new position
                  -> Rotation -- ^ old rotation
                  -> Rotation -- ^ new rotation
                  -> Time
                  -> Velocity
calculateVelocity (Position px1 py1) (Position px2 py2) (Rotation r1) (Rotation r2) time =
    Velocity {
      velocitySpeed    = (sqrt $ (px2 - px1)**2 + (py2 - py1)**2) / time
    , velocityRotation = (r2 - r1) / time
    }
