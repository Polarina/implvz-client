module Game.Implvz.Objects.Player (
    Player (..)
  , setPlayerHealth
) where

import Game.Implvz.Types

data Player = Player {
      playerName             :: !PlayerName
    , playerHealth           :: !Float
    , playerLastHealthUpdate :: !Time
    } deriving (Eq, Show)

setPlayerHealth :: Time -> Float -> Player -> Player
setPlayerHealth playerLastHealthUpdate playerHealth player =
    player {
        playerHealth
      , playerLastHealthUpdate
      }
