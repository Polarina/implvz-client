module Game.Implvz.Objects.Entity (
    Entity (..)
  , isPlayer
  , isProjectile
  , isPartial
  , asPlayer
) where

import Game.Implvz.Attributes
import Game.Implvz.Components
import Game.Implvz.Objects.Player

data Entity = EntityPlayer {
              entityPlayer       :: !Player
            , entityDimension    :: !Dimension
            , entityPositionPrev :: !Position
            , entityPosition     :: !Position
            , entityRotationPrev :: !Rotation
            , entityRotation     :: !Rotation
            , entityVelocity     :: !Velocity
            }
            | EntityProjectile {
              entityDimension    :: !Dimension
            , entityPositionPrev :: !Position
            , entityPosition     :: !Position
            , entityRotationPrev :: !Rotation
            , entityRotation     :: !Rotation
            , entityVelocity     :: !Velocity
            }
            | EntityPartialPlayer {
              entityPlayer :: !Player
            } deriving (Eq, Show)

instance DimensionA Entity where
    getDimension = entityDimension
    setDimension entityDimension = \x -> x { entityDimension }

instance PositionA Entity where
    getPosition = entityPosition
    setPosition entityPosition = \x -> x { entityPosition }

instance RotationA Entity where
    getRotation = entityRotation
    setRotation entityRotation = \x -> x { entityRotation }

instance VelocityA Entity where
    getVelocity = entityVelocity
    setVelocity entityVelocity = \x -> x { entityVelocity }

instance MovementC Entity

isPlayer :: Entity -> Bool
isPlayer (EntityPlayer _ _ _ _ _ _ _) = True
isPlayer _ = False

isProjectile :: Entity -> Bool
isProjectile (EntityProjectile _ _ _ _ _ _) = True
isProjectile _ = False

isPartial :: Entity -> Bool
isPartial (EntityPartialPlayer _) = True
isPartial _ = False

asPlayer :: Entity -> Player
asPlayer (EntityPlayer player _ _ _ _ _ _) = player
asPlayer _ = error "entity not a player when requested"
