module Game.Implvz.Objects.Map (
    Map (..)
  , getEntityByID
  , getEntityByName
  , setEntityByID
  , setEntityByName
  , removeEntityByID
  , isColliding
) where

import Codec.Picture
import qualified Data.Map as M
import Game.Implvz.Attributes
import Game.Implvz.Objects.Entity
import Game.Implvz.Objects.Player
import Game.Implvz.Protocol ()
import Game.Implvz.Types

data Map = Map {
    mapDimension :: !Dimension
  , mapImage     :: !(Maybe (Image PixelRGBA8))
  , mapEntities  :: !(M.Map EntityID Entity)
  , mapPlayers   :: !(M.Map PlayerName EntityID)
  } deriving (Eq, Show)

instance DimensionA Map where
    getDimension = mapDimension
    setDimension mapDimension = \x -> x { mapDimension }

getEntityByID :: EntityID -> Map -> Maybe Entity
getEntityByID eid Map{..} = M.lookup eid mapEntities

getEntityByName :: PlayerName -> Map -> Maybe Entity
getEntityByName name m@(Map{..}) = do
    eid <- M.lookup name mapPlayers
    getEntityByID eid m

setEntityByID :: EntityID -> Entity -> Map -> Map
setEntityByID eid entity m =
    m {
      mapEntities = M.insert eid entity $ mapEntities m
    , mapPlayers  = case entity of
          EntityPlayer (Player name _ _) _ _ _ _ _ _ -> M.insert name eid $ mapPlayers m
          _ -> mapPlayers m -- fix
    }

setEntityByName :: PlayerName -> Entity -> Map -> Map
setEntityByName name entity m = let (Just eid) = M.lookup name $ mapPlayers m in setEntityByID eid entity m

removeEntityByID :: EntityID -> Map -> Map
removeEntityByID eid m =
    m {
        mapEntities = M.delete eid $ mapEntities m
      , mapPlayers  = maybe (mapPlayers m) id $
            M.lookup eid (mapEntities m) >>= name >>= \x -> return (M.delete x $ mapPlayers m)
      }
    where
    name :: Entity -> Maybe PlayerName
    name (EntityPlayer player _ _ _ _ _ _) = Just $ playerName player
    name _ = Nothing

isColliding :: Float -- ^ radius
            -> Position
            -> Map
            -> Bool
isColliding radius Position{..} (Map _ (Just image) _ _)
    | sx < 0 || sx >= width  = True
    | sy < 0 || sy >= height = True
    | ex < 0 || ex >= width  = True
    | ey < 0 || ey >= height = True
    | otherwise = not . null . filter (== True) $ map (\(x, y) ->
        check ((fromIntegral x) + 0.5) ((fromIntegral y) + 0.5) $
            pixelAt image x $ height - y - 1) coords
    where
    sx = floor $ positionX - radius
    sy = floor $ positionY - radius
    ex = ceiling $ positionX + radius
    ey = ceiling $ positionY + radius
    width  = imageWidth image
    height = imageHeight image
    coords = [(a, b) | a <- [sx..ex], b <- [sy..ey]]
    check x y (PixelRGBA8 0x00 0x00 0x00 0xff)
        | dx > 0.5 + radius = False
        | dy > 0.5 + radius = False
        | dx <= 0.5 = True
        | dy <= 0.5 = True
        | (dx - 0.5)**2 + (dy - 0.5)**2 <= radius**2 = True
        where
        dx = abs $ positionX - x
        dy = abs $ positionY - y
    check _ _ _ = False
   
isColliding _ _ _ = False
