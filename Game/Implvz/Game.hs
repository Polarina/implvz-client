module Game.Implvz.Game (
    GameState (..)
  , game
) where

import Codec.Picture
import Control.Monad
import Data.Char
import Data.List
import qualified Data.Map as M
import Data.Maybe
import qualified Data.Set as Set
import Game.Implvz.Attributes
import Game.Implvz.Components
import Game.Implvz.Objects
import Game.Implvz.Physical
import Game.Implvz.Protocol
import Game.Implvz.Types

data GameState = GameState {
    gameElapsedTime    :: !Time
  , gameCurrentPlayer  :: !PlayerName
  , gameKeysDown       :: !(Set.Set Key)
  , gameViewport       :: !Dimension
  , gameMap            :: !Map
  , gameIsConnected    :: !(Maybe String)
  , gameAllowTyping    :: !Bool
  , gameTypeBuffer     :: !String
  } deriving (Eq, Show)

setMap :: GameState -> Map -> GameState
setMap gs m = gs { gameMap = m }

processServer :: ServerMessage -> GameState -> GameState
processServer msg gs = case msg of
    SClientConnected clients -> foldr
        (\(eid, name) x@(GameState{..}) -> setMap x (setEntityByID eid (EntityPartialPlayer (Player name 1.0 0.0)) gameMap))
        gs
        clients

    SUnregisterEntity eid -> setMap gs . removeEntityByID eid $ gameMap gs

    SHealth healths ->
        let f = \(name, health) x@(GameState{..}) -> case getEntityByName name gameMap of
                        Just entity -> setMap x $ setEntityByName name (entity { entityPlayer = setPlayerHealth gameElapsedTime health $ entityPlayer entity }) gameMap
                        Nothing -> error "entity not found"
        in foldr f gs healths

    SMap m -> gs {
        gameMap = (gameMap gs) {
            mapDimension = Dimension {
                dimensionWidth  = realToFrac $ imageWidth m
              , dimensionHeight = realToFrac $ imageHeight m
              }
          , mapImage = Just m
          }
      }

    SMove eid pos rot -> case getEntityByID eid $ gameMap gs of
        Just entity -> case isPartial entity of
            True ->
                setMap gs $ setEntityByID eid (EntityPlayer (entityPlayer entity) (Dimension 0.75 0.75) pos pos rot rot (Velocity 0.0 0.0)) $ gameMap gs
            False -> setMap gs $ setEntityByID eid
                (setPosition pos .
                 setRotation rot .
                 (\x -> x {
                     entityPositionPrev = getPosition entity
                   , entityRotationPrev = getRotation entity
                   }) $ entity) $ gameMap gs
        Nothing ->
            setMap gs $ setEntityByID eid (EntityProjectile (Dimension 0.35 0.35) pos pos rot rot (Velocity 12.5 0.0)) $ gameMap gs

    SKick reason -> gs { gameIsConnected = Just . map toUpper $ "KICKED FROM SERVER\n" ++ show reason }

    _ -> gs

processPhysical :: PhysicalMessage -> GameState -> (GameState, [ClientMessage])
processPhysical msg gs@(GameState{..}) = case msg of
    PKeyboard key Down -> let ignore = Set.member key gameKeysDown in
            case ignore of
                True -> (gs, [])
                False -> applyControls key Down $ gs { gameKeysDown = Set.insert key gameKeysDown }

    PKeyboard key Up -> let ignore = Set.notMember key gameKeysDown in
        case ignore of
            True -> (gs, [])
            False -> applyControls key Up $ gs { gameKeysDown = Set.delete key gameKeysDown }

    PMotion _ _ -> (gs, [])

    PReshape width height -> (gs { gameViewport = Dimension {
        dimensionWidth  = fromIntegral width
      , dimensionHeight = fromIntegral height
      } }, [])

    PConnected -> (gs { gameIsConnected = Nothing }, [])

    PDisconnected reason -> (gs { gameIsConnected = Just reason }, [])

applyControls :: Key -> KeyState -> GameState -> (GameState, [ClientMessage])
applyControls key keystate gs@(GameState{..}) | not gameAllowTyping = case getEntityByName gameCurrentPlayer gameMap of
    Just entity ->
        let (velocity, messages) = apply key keystate
        in (gs { gameMap = setEntityByName gameCurrentPlayer (setVelocity (getVelocity entity + velocity) entity) gameMap }, messages)
    Nothing -> (gs, [])
    where
    speed    = 6.0
    rotation = pi
    apply k s = case (k, s) of
        (Char 'd', st) -> apply (SpecialKey KeyRight) st
        (Char 'e', st) -> apply (SpecialKey KeyRight) st
        (Char 'D', st) -> apply (SpecialKey KeyRight) st
        (Char 'E', st) -> apply (SpecialKey KeyRight) st
        (SpecialKey KeyRight, Down) -> (Velocity 0.0 (-rotation), [])
        (SpecialKey KeyRight, Up)   -> (Velocity 0.0 rotation, [])

        (Char 'w', st) -> apply (SpecialKey KeyUp) st
        (Char ',', st) -> apply (SpecialKey KeyUp) st
        (Char 'W', st) -> apply (SpecialKey KeyUp) st
        (SpecialKey KeyUp, Down)    -> (Velocity speed 0.0, [])
        (SpecialKey KeyUp, Up)      -> (Velocity (-speed) 0.0, [])

        (Char 'a', st) -> apply (SpecialKey KeyLeft) st
        (Char 'A', st) -> apply (SpecialKey KeyLeft) st
        (SpecialKey KeyLeft, Down)  -> (Velocity 0.0 rotation, [])
        (SpecialKey KeyLeft, Up)    -> (Velocity 0.0 (-rotation), [])

        (Char 's', st) -> apply (SpecialKey KeyDown) st
        (Char 'o', st) -> apply (SpecialKey KeyDown) st
        (Char 'S', st) -> apply (SpecialKey KeyDown) st
        (Char 'O', st) -> apply (SpecialKey KeyDown) st
        (SpecialKey KeyDown, Down)  -> (Velocity (-speed) 0.0, [])
        (SpecialKey KeyDown, Up)    -> (Velocity speed 0.0, [])

        (MouseButton LeftButton, Down) -> apply (Char ' ') Down
        (Char ' ', Down) -> (Velocity 0.0 0.0, [CFire])
        _ -> (Velocity 0.0 0.0, [])
applyControls key Down gs@(GameState{..}) = case key of
    Char a | a >= 'a' && a <= 'z' -> applyControls (Char $ toUpper a) Down gs
    Char a | a >= 'A' && a <= 'Z' || a >= '0' && a <= '9' || a == '.' ->
        (gs { gameTypeBuffer = gameTypeBuffer ++ [a] }, [])
    Char '\b' ->
        (gs { gameTypeBuffer = if gameTypeBuffer == "" then "" else init gameTypeBuffer }, [])
    Char '\r' ->
        (gs { gameAllowTyping = False
            , gameIsConnected = Just $ "CONNECTING TO " ++ gameTypeBuffer ++ "..." }
        , [])
    _ -> (gs, [])
applyControls _ _ gs = (gs, [])

physicsPrecision :: Time
physicsPrecision = 1 / 8192

knockbackTime :: Time
knockbackTime = 1 / 512

findTemporal :: Time -- ^ epsilon
             -> Time -- ^ start
             -> Time -- ^ end
             -> (Time -> Maybe a)
             -> Maybe a
findTemporal epsilon start end f
    | end - start <= epsilon = f start
    | isJust f' = findTemporal epsilon m end f
    | otherwise = findTemporal epsilon start m f
    where
    m  = (start + end) * 0.5
    f' = f m

simulateEntity :: Time
               -> Map
               -> Entity
               -> Entity
simulateEntity t gameMap s = let m = move' t s in setRotation (getRotation m) s'
    where
    Dimension{..} = getDimension s
    rad = (dimensionWidth + dimensionHeight) * 0.25
    (s', _) = simulateEntity' rad t gameMap True id s

simulateEntity' :: Float                  -- ^ radius
                -> Time                   -- ^ time to simulate
                -> Map                    -- ^ map for collision
                -> Bool                   -- ^ slide
                -> (Position -> Position) -- ^ position modifier
                -> Entity                 -- ^ entity
                -> (Entity, Time)         -- ^ (simulated entity, time simulated untill impass)
simulateEntity' rad t gameMap shouldSlide f s
    | t <= knockbackTime = (s, 0)
    | not $ isColliding rad (f . getPosition $ move' t s) gameMap = (apply t s, t)
    | t <= physicsPrecision = (s, 0)
    | otherwise =
        case findTemporal physicsPrecision 0.0 t $
                \x -> let s' =  apply x s
                      in guard (not $ isColliding rad (getPosition s') gameMap) >> Just x of
            Just t' -> let
                    t'' = max 0 $ t' - knockbackTime
                    st = apply t'' s
                in if shouldSlide then
                       let (st1, t1) = simulateEntity' rad (t - t'') gameMap False (\(Position x _) -> Position x (positionY $ getPosition st)) st
                           (st2, t2) = simulateEntity' rad ((t - t'') - t1) gameMap False (\(Position _ y) -> Position (positionX $ getPosition st) y) st1
                       in (st2, t - t2)
                   else
                       (st, t'')
            Nothing -> (s, 0)
    where
    apply :: Time -> Entity -> Entity
    apply time entity = let m = move' time entity in (\y -> setPosition y m) . f . getPosition $ m

game :: Time
     -> [PhysicalMessage]
     -> [ServerMessage]
     -> GameState
     -> (GameState, [ClientMessage])
game t pm sm gs = compose (gs, [])
    [ \(x, m) -> (x { gameElapsedTime = gameElapsedTime x + t }, m)
    , \(x, m) -> foldl' (\(gs', msgs') pm' -> let (a, b) = processPhysical pm' gs' in (a, msgs' ++ b)) (x, m) pm
    , \(x, m) -> let eids = M.keys . mapEntities $ gameMap x in
        (compose x $ map (\eid x' -> simulate True x' eid) eids, m)
    , \(x, m) -> (foldl' (\gs' sm' -> processServer sm' gs') x sm, m)
    , \(x, m) -> let eids = M.keys . mapEntities $ gameMap x in
        (compose x $ map (\eid x' -> simulate False x' eid) eids, m)
    --, \(x, m) -> (foldl' (\gs' sm' -> processServer sm' gs') x sm, m)
    , \(x, m) -> let entity' = getEntityByName (gameCurrentPlayer x) (gameMap x) in case entity' of
        Just entity -> let pos = entityPosition entity; rot = entityRotation entity in
            (x, if pos /= entityPositionPrev entity || rot /= entityRotationPrev entity
                    then m ++ [CMove (entityPosition entity) (entityRotation entity)]
                    else m)
        Nothing -> (x, m)
    ]
    where
    simulate :: Bool -> GameState -> EntityID -> GameState
    simulate pre gs' eid =
        let (Just entity) = getEntityByID eid (gameMap gs') in
        setMap gs' $ setEntityByID eid (case True of
            _ | not pre && (isPlayer entity && playerName (asPlayer entity) == gameCurrentPlayer gs' || isProjectile entity) ->
                simulateEntity t (gameMap gs') $ entity {
                    entityPositionPrev = entityPosition entity
                  , entityRotationPrev = entityRotation entity
                  }
              | pre && isPlayer entity ->
                entity {
                    entityPositionPrev = entityPosition entity
                  , entityRotationPrev = entityRotation entity
                  }
              | otherwise -> entity
        ) $ gameMap gs'
