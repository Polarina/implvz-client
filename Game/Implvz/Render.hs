module Game.Implvz.Render (
    RenderState (..)
  , initialRenderState
  , render
) where

import Codec.Picture
import Control.Applicative
import Control.Monad.ST
import qualified Data.ByteString as S
import qualified Data.ByteString.Char8 as S8
import qualified Data.ByteString.Unsafe as SU
import Data.Bits
import Data.Char
import Data.Fixed
import Data.Foldable hiding (maximumBy)
import Data.Function
import Data.List hiding (concatMap)
import qualified Data.Map as M
import Data.STRef
import qualified Data.Vector.Storable as VS
import qualified Data.Vector.Storable.Mutable as VSM
import Foreign.C.String
import Foreign.ForeignPtr.Safe
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.Storable
import Game.Implvz.Attributes
import Game.Implvz.Game
import Game.Implvz.Objects
import Game.Implvz.Types
import Graphics.Rendering.OpenGL.Raw
import System.IO
import Prelude hiding (concatMap, map, mapM_)

data RenderState = RenderState {
    mapData            :: !(Maybe (Image PixelRGBA8))

  -- Textures
  , texStarfield       :: !GLuint
  , texUniversal       :: !GLuint
  , texFont            :: !GLuint

  -- Programs
  , programShader      :: !GLuint

  -- Uniforms
  , ufProjectionMatrix :: !GLint
  , ufOrthoPosition    :: !GLint
  , ufTransform        :: !GLint
  , ufTexTransform     :: !GLint
  , ufScale            :: !GLint
  , ufRotate           :: !GLint
  , ufTex              :: !GLint
  , ufColorMultiplier  :: !GLint

  -- VAOs
  , vaoBullet          :: !GLuint
  , vaoMap             :: !GLuint
  , vaoStarfield       :: !GLuint
  , vaoPlayer          :: !GLuint
  , vaoLetter          :: !GLuint

  -- VAOs vertices counts
  , verticesBullet     :: !GLsizei
  , verticesMap        :: !GLsizei
  , verticesStarfield  :: !GLsizei
  , verticesPlayer     :: !GLsizei
  , verticesLetter     :: !GLsizei

  -- font
  , letterLookup       :: !(M.Map Char GLfloat)
  } deriving (Eq, Show)

loadImage :: FilePath -> IO DynamicImage
loadImage name = do
    image <- readPng name
    case image of
        Left err  -> error err
        Right img -> return img

processMap :: Image PixelRGBA8 -> (VS.Vector GLfloat, VS.Vector GLint)
processMap img =
    runST $ do
        (v, i) <- scan
        (,) <$> pure v <*> VS.unsafeFreeze i
    where
    nums =
        foldr' cnt 0 [(a-1, b-1) | a <- [1..width], b <- [1..height]]
    cnt (x, y) c = case pixelAt img x y of
        PixelRGBA8 0x00 0x00 0x00 0xff -> c + 30
        PixelRGBA8 0xff 0xff 0xff 0xff -> c + 6
        PixelRGBA8 0xc8 0xc8 0xc8 0xff -> c
        PixelRGBA8 r g b a -> error $ "unknown colour in map " ++ show (r, g, b, a)

    width  = imageWidth img
    height = imageHeight img

    lkp :: (Ord k) => STRef s (M.Map k GLint) -> k -> ST s GLint
    lkp m k = do
        m' <- readSTRef m

        case M.lookup k m' of
            Just c -> return c
            Nothing -> let c = fromIntegral $ M.size m' in do
                writeSTRef m $ M.insert k c m'
                return c

    scan :: ST s (VS.Vector GLfloat, VSM.STVector s GLint)
    scan = do
        idx  <- newSTRef M.empty
        v    <- VSM.replicate nums (-1)
        voff <- newSTRef 0

        forM_ [0..width*height-1] $ \poff -> do
            off <- readSTRef voff

            let x  = poff `rem` width
                y  = poff `div` width
                p  = pixelAt img x y
                x' = realToFrac x
                y' = realToFrac $ height - y - 1

            case p of
                PixelRGBA8 0x00 0x00 0x00 0xff -> do
                    let ts1 = 0.25
                        tt1 = 0.0
                        ts2 = 0.5
                        tt2 = 0.25

                        ss1 = 0.25
                        st1 = 0.25
                        ss2 = 0.5
                        st2 = 0.5
                    mapM_ (\(i, w) -> lkp idx w >>= VSM.unsafeWrite v (off + i)) $ zip [0..] [
                        -- top-facing
                          [x'    , y'    , 1.0, ts1, tt2]
                        , [x' + 1, y'    , 1.0, ts2, tt2]
                        , [x'    , y' + 1, 1.0, ts1, tt1]

                        , [x'    , y' + 1, 1.0, ts1, tt1]
                        , [x' + 1, y'    , 1.0, ts2, tt2]
                        , [x' + 1, y' + 1, 1.0, ts2, tt1]

                        -- east-facing
                        , [x' + 1, y'    , 0.0, ss1, st2]
                        , [x' + 1, y' + 1, 0.0, ss2, st2]
                        , [x' + 1, y'    , 1.0, ss1, st1]

                        , [x' + 1, y'    , 1.0, ss1, st1]
                        , [x' + 1, y' + 1, 0.0, ss2, st2]
                        , [x' + 1, y' + 1, 1.0, ss2, st1]

                        -- north-facing
                        , [x' + 1, y' + 1, 0.0, ss1, st2]
                        , [x'    , y' + 1, 0.0, ss2, st2]
                        , [x' + 1, y' + 1, 1.0, ss1, st1]

                        , [x' + 1, y' + 1, 1.0, ss1, st1]
                        , [x'    , y' + 1, 0.0, ss2, st2]
                        , [x'    , y' + 1, 1.0, ss2, st1]

                        -- west-facing
                        , [x'    , y' + 1, 0.0, ss1, st2]
                        , [x'    , y'    , 0.0, ss2, st2]
                        , [x'    , y' + 1, 1.0, ss1, st1]

                        , [x'    , y' + 1, 1.0, ss1, st1]
                        , [x'    , y'    , 0.0, ss2, st2]
                        , [x'    , y'    , 1.0, ss2, st1]

                        -- south-facing
                        , [x'    , y'    , 0.0, ss1, st2]
                        , [x' + 1, y'    , 0.0, ss2, st2]
                        , [x'    , y'    , 1.0, ss1, st1]

                        , [x'    , y'    , 1.0, ss1, st1]
                        , [x' + 1, y'    , 0.0, ss2, st2]
                        , [x' + 1, y'    , 1.0, ss2, st1]
                        ]
                    writeSTRef voff $ off + 30
                PixelRGBA8 0xff 0xff 0xff 0xff -> do
                    let s = ((x' + 0.5) `mod'` 64) / 256
                        t = ((y' - 0.5) `mod'` 64) / 256
                    mapM_ (\(i, w) -> lkp idx w >>= VSM.unsafeWrite v (off + i)) $ zip [0..] [
                          [x'    , y'    , 0.0, s, t]
                        , [x' + 1, y'    , 0.0, s, t]
                        , [x'    , y' + 1, 0.0, s, t]
                       
                        , [x'    , y' + 1, 0.0, s, t]
                        , [x' + 1, y'    , 0.0, s, t]
                        , [x' + 1, y' + 1, 0.0, s, t]
                        ]
                    writeSTRef voff $ off + 6
                _ -> return ()

        idx' <- readSTRef idx
        return (VS.fromList . concatMap fst . sortBy (compare `on` snd) $ M.toList idx', v)

starfieldCoords :: VS.Vector GLfloat
starfieldCoords = VS.fromList [
    -- vertex
      (-1.0), (-1.0), 0.0
    ,   1.0,  (-1.0), 0.0
    , (-1.0),   1.0,  0.0
    ,   1.0,    1.0,  0.0
    -- texture
    , 0.0, 1.0
    , 1.0, 1.0
    , 0.0, 0.0
    , 1.0, 0.0
    ]

bulletCoords :: VS.Vector GLfloat
bulletCoords = VS.fromList [
      -- vertex
      (-1.0), (-1.0), 0.5
    ,   1.0,  (-1.0), 0.5
    , (-1.0),   1.0,  0.5
    ,   1.0,    1.0,  0.5
    -- texture
    , 0.0,  0.75
    , 0.25, 0.75
    , 0.0,  0.5
    , 0.25, 0.5
    ]

playerCoords :: VS.Vector GLfloat
playerCoords = VS.fromList [
    -- vertex
      (-1.0), (-1.0), 0.5
    ,   1.0,  (-1.0), 0.5
    , (-1.0),   1.0,  0.5
    ,   1.0,    1.0,  0.5
    -- texture
    , 0.0,  0.5
    , 0.25, 0.5
    , 0.0,  0.25
    , 0.25, 0.25
    ]

letterCoords :: VS.Vector GLfloat
letterCoords = VS.fromList [
    -- vertex
      0.0, 0.0, 0.0
    , 1.0, 0.0, 0.0
    , 0.0, 1.0, 0.0
    , 1.0, 1.0, 0.0
    -- texture
    , 0.0,   9/16
    , 4/256, 9/16
    , 0.0,   0.0
    , 4/256, 0.0
    ]

orthographicMatrix :: GLfloat -> GLfloat -> GLfloat -> GLfloat -> VS.Vector GLfloat
orthographicMatrix right top left bottom = VS.fromList
    [  2/(right-left),             0,                          0,                         0
    ,  0,                          2/(top-bottom),             0,                         0
    ,  0,                          0,                         -2/(zfar-znear),            0
    , -(right+left)/(right-left), -(top+bottom)/(top-bottom), -(zfar+znear)/(zfar-znear), 1
    ]
    where
    znear = 30.0
    zfar  = 31.0001

perspectiveMatrix :: GLfloat -> VS.Vector GLfloat
perspectiveMatrix aspect = VS.fromList
    [ xymax / aspect, 0,     0,                 0
    , 0,              xymax, 0,                 0
    , 0,              0,     zfar/depth,       -1
    , 0,              0,     znear*zfar/depth,  0
    ]
    where
    depth = znear - zfar
    znear = 30.0
    zfar  = 31.0001
    xymax = 1 / tan(45 * 0.5 * (pi / 180))

loadMatrix :: VS.Vector GLfloat -> GLint -> IO ()
loadMatrix matrix uniform = VS.unsafeWith matrix $
    glUniformMatrix4fv uniform 1 $ fromIntegral gl_FALSE

mkProgram :: S.ByteString -> S.ByteString -> IO GLuint
mkProgram vertex fragment = do
    program <- glCreateProgram

    forM_ [(gl_VERTEX_SHADER, vertex), (gl_FRAGMENT_SHADER, fragment)] $ \(typ, src) -> do
        shader <- glCreateShader typ

        alloca $ \ptr1 -> alloca $ \ptr2 -> SU.unsafeUseAsCStringLen src $ \(str, len) -> do
            poke ptr1 str
            poke ptr2 $ fromIntegral len
            glShaderSource shader 1 (castPtr ptr1) ptr2

        glCompileShader shader

        checkStatus
            (glGetShaderiv shader gl_COMPILE_STATUS)
            (glGetShaderiv shader gl_INFO_LOG_LENGTH)
            (glGetShaderInfoLog shader)
            (glDeleteShader shader)
            (glAttachShader program shader >> glDeleteShader shader)
            "shader compilation failure"

    withCString "in_Position" $ glBindAttribLocation program 0 . castPtr
    withCString "in_Texcoord" $ glBindAttribLocation program 1 . castPtr
    glLinkProgram program

    glUseProgram program
    loc <- withCString "tex" $ glGetUniformLocation program . castPtr
    glUniform1i loc 1

    checkStatus
        (glGetProgramiv program gl_LINK_STATUS)
        (glGetProgramiv program gl_INFO_LOG_LENGTH)
        (glGetProgramInfoLog program)
        (glDeleteProgram program)
        (return ())
        "program linking failure"

    return program
    where
    checkStatus getStatus getLength getLog doFail doSuccess err = do
        status <- alloca $ \ptr -> do
            _ <- getStatus ptr
            peek ptr

        case status of
            0 -> do
                loglen <- alloca $ \ptr -> do
                    _ <- getLength ptr
                    peek ptr

                logptr <- mallocBytes $ (fromIntegral loglen) + 1
                _ <- getLog (fromIntegral loglen) nullPtr logptr

                -- use SE.unsafePackMallocCStringLen when migrating to bytestring-0.10
                logs <- SU.unsafePackMallocCString $ castPtr logptr

                _ <- doFail
                S8.putStrLn logs
                fail err
            _ -> doSuccess

mkTexture :: DynamicImage -> GLenum -> GLenum -> GLenum -> IO GLuint
mkTexture image active wrap texFilter = do
    tex <- alloca $ \ptr -> do
        glGenTextures 1 ptr
        peek ptr

    glActiveTexture active
    glBindTexture gl_TEXTURE_2D tex
    glTexParameteri gl_TEXTURE_2D gl_TEXTURE_WRAP_S     $ fromIntegral wrap
    glTexParameteri gl_TEXTURE_2D gl_TEXTURE_WRAP_T     $ fromIntegral wrap
    glTexParameteri gl_TEXTURE_2D gl_TEXTURE_MAG_FILTER $ fromIntegral texFilter
    glTexParameteri gl_TEXTURE_2D gl_TEXTURE_MIN_FILTER $ fromIntegral texFilter

    case image of
        ImageRGB8 img  -> doTex gl_RGB  img
        ImageRGBA8 img -> doTex gl_RGBA img
        _              -> error "unsupported texture format of {RGB8; RGBA8}"

    glGenerateMipmap gl_TEXTURE_2D

    return tex
    where
    doTex typ img = let (fptr, _) = VS.unsafeToForeignPtr0 $ imageData img in
        withForeignPtr fptr $ glTexImage2D
            gl_TEXTURE_2D
            0
            (fromIntegral typ)
            (fromIntegral $ imageWidth img)
            (fromIntegral $ imageHeight img)
            0
            typ
            gl_UNSIGNED_BYTE

mkMap :: IO (GLuint, GLsizei)
mkMap = do
    (vao, vbo) <- alloca $ \ptr -> do
        vao <- glGenVertexArrays 1 ptr >> peek ptr
        vbo <- glGenBuffers 1 ptr >> peek ptr
        return (vao, vbo)

    (vbo2) <- alloca $ \ptr -> glGenBuffers 1 ptr >> peek ptr

    glBindVertexArray vao
    glBindBuffer gl_ARRAY_BUFFER vbo
    glBindBuffer gl_ELEMENT_ARRAY_BUFFER vbo2

    glVertexAttribPointer 0 3 gl_FLOAT (fromIntegral gl_FALSE) 20 nullPtr
    glVertexAttribPointer 1 2 gl_FLOAT (fromIntegral gl_FALSE) 20 . intPtrToPtr $ 3 * 4

    glEnableVertexAttribArray 0
    glEnableVertexAttribArray 1

    return (vao, 0)

mkStarfield :: IO (GLuint, GLsizei)
mkStarfield = do
    (vao, vbo) <- alloca $ \ptr -> do
        vao <- glGenVertexArrays 1 ptr >> peek ptr
        vbo <- glGenBuffers 1 ptr >> peek ptr
        return (vao, vbo)

    glBindVertexArray vao
    glBindBuffer gl_ARRAY_BUFFER vbo

    VS.unsafeWith starfieldCoords $ \ptr ->
        glBufferData gl_ARRAY_BUFFER (fromIntegral $ VS.length starfieldCoords * 4) ptr gl_STATIC_DRAW

    glVertexAttribPointer 0 3 gl_FLOAT (fromIntegral gl_FALSE) 0 nullPtr
    glVertexAttribPointer 1 2 gl_FLOAT (fromIntegral gl_FALSE) 0 . intPtrToPtr $ 12 * 4

    glEnableVertexAttribArray 0
    glEnableVertexAttribArray 1

    return (vao, 4)

mkBullet :: IO (GLuint, GLsizei)
mkBullet = do
    (vao, vbo) <- alloca $ \ptr -> do
        vao <- glGenVertexArrays 1 ptr >> peek ptr
        vbo <- glGenBuffers 1 ptr >> peek ptr
        return (vao, vbo)

    glBindVertexArray vao
    glBindBuffer gl_ARRAY_BUFFER vbo

    VS.unsafeWith bulletCoords $ \ptr ->
        glBufferData gl_ARRAY_BUFFER (fromIntegral $ VS.length bulletCoords * 4) ptr gl_STATIC_DRAW

    glVertexAttribPointer 0 3 gl_FLOAT (fromIntegral gl_FALSE) 0 nullPtr
    glVertexAttribPointer 1 2 gl_FLOAT (fromIntegral gl_FALSE) 0 . intPtrToPtr $ 12 * 4

    glEnableVertexAttribArray 0
    glEnableVertexAttribArray 1

    return (vao, 4)

mkPlayer :: IO (GLuint, GLsizei)
mkPlayer = do
    (vao, vbo) <- alloca $ \ptr -> do
        vao <- glGenVertexArrays 1 ptr >> peek ptr
        vbo <- glGenBuffers 1 ptr >> peek ptr
        return (vao, vbo)

    glBindVertexArray vao
    glBindBuffer gl_ARRAY_BUFFER vbo

    VS.unsafeWith playerCoords $ \ptr ->
        glBufferData gl_ARRAY_BUFFER (fromIntegral $ VS.length playerCoords * 4) ptr gl_STATIC_DRAW

    glVertexAttribPointer 0 3 gl_FLOAT (fromIntegral gl_FALSE) 0 nullPtr
    glVertexAttribPointer 1 2 gl_FLOAT (fromIntegral gl_FALSE) 0 . intPtrToPtr $ 12 * 4

    glEnableVertexAttribArray 0
    glEnableVertexAttribArray 1

    return (vao, 4)

mkLetter :: IO (GLuint, GLsizei)
mkLetter = do
    (vao, vbo) <- alloca $ \ptr -> do
        vao <- glGenVertexArrays 1 ptr >> peek ptr
        vbo <- glGenBuffers 1 ptr >> peek ptr
        return (vao, vbo)

    glBindVertexArray vao
    glBindBuffer gl_ARRAY_BUFFER vbo

    VS.unsafeWith letterCoords $ \ptr ->
        glBufferData gl_ARRAY_BUFFER (fromIntegral $ VS.length letterCoords * 4) ptr gl_STATIC_DRAW

    glVertexAttribPointer 0 3 gl_FLOAT (fromIntegral gl_FALSE) 0 nullPtr
    glVertexAttribPointer 1 2 gl_FLOAT (fromIntegral gl_FALSE) 0 . intPtrToPtr $ 12 * 4

    glEnableVertexAttribArray 0
    glEnableVertexAttribArray 1

    return (vao, 4)

initialRenderState :: IO RenderState
initialRenderState = do
    glEnable gl_BLEND
    glEnable gl_DEPTH_TEST
    glEnable gl_CULL_FACE

    glBlendFunc gl_SRC_ALPHA gl_ONE_MINUS_SRC_ALPHA
    glPrimitiveRestartIndex $ -1

    vertexShader       <- S.readFile "res/vertex.glsl"
    fragmentShader     <- S.readFile "res/fragment.glsl"
    programShader      <- mkProgram vertexShader fragmentShader

    starfieldImage     <- loadImage "res/starfield.png"
    universalImage     <- loadImage "res/texture.png"
    fontImage          <- loadImage "res/font.png"

    texStarfield       <- mkTexture starfieldImage gl_TEXTURE1 gl_CLAMP gl_LINEAR_MIPMAP_LINEAR
    texUniversal       <- mkTexture universalImage gl_TEXTURE2 gl_CLAMP gl_LINEAR
    texFont            <- mkTexture fontImage      gl_TEXTURE3 gl_CLAMP gl_NEAREST

    ufProjectionMatrix <- withCString "projectionMatrix" $ glGetUniformLocation programShader . castPtr
    ufOrthoPosition    <- withCString "orthoPosition"    $ glGetUniformLocation programShader . castPtr
    ufTransform        <- withCString "transform"        $ glGetUniformLocation programShader . castPtr
    ufTexTransform     <- withCString "texTransform"     $ glGetUniformLocation programShader . castPtr
    ufScale            <- withCString "scale"            $ glGetUniformLocation programShader . castPtr
    ufRotate           <- withCString "rotate"           $ glGetUniformLocation programShader . castPtr
    ufTex              <- withCString "tex"              $ glGetUniformLocation programShader . castPtr
    ufColorMultiplier  <- withCString "colorMultiplier"  $ glGetUniformLocation programShader . castPtr

    (vaoBullet,    verticesBullet)    <- mkBullet
    (vaoPlayer,    verticesPlayer)    <- mkPlayer
    (vaoStarfield, verticesStarfield) <- mkStarfield
    (vaoLetter,    verticesLetter)    <- mkLetter
    (vaoMap, verticesMap)       <- mkMap

    mapData <- return Nothing

    letters <- readFile "res/font.txt"
    let letterLookup = M.fromList $ zip letters $ map (\c -> c * (4 / 256)) [0..]

    return $ RenderState {..}

renderCamera :: Float -> Position -> RenderState -> IO ()
renderCamera aspect Position{..} RenderState{..} = do
    loadMatrix (perspectiveMatrix $ realToFrac aspect) ufProjectionMatrix
    glUniform2f ufOrthoPosition (realToFrac positionX) (realToFrac positionY)

renderMap :: Map -> RenderState -> IO RenderState
renderMap Map{..} rs@(RenderState{..}) = do
    glUniform2f ufTransform 0.0 0.0
    glUniform2f ufScale     1.0 1.0
    glUniform1f ufRotate    0.0
    glUniform1i ufTex       2
    glBindVertexArray vaoMap

    (vertices, rs') <- case mapImage of
        Just img | mapImage /= mapData -> do
            let (vec, idx) = processMap img
                (fptr1, _) = VS.unsafeToForeignPtr0 vec
                (fptr2, _) = VS.unsafeToForeignPtr0 idx

            withForeignPtr fptr1 $ \ptr ->
                glBufferData gl_ARRAY_BUFFER (fromIntegral $ 4 * VS.length vec) ptr gl_STATIC_DRAW

            withForeignPtr fptr2 $ \ptr ->
                glBufferData gl_ELEMENT_ARRAY_BUFFER (fromIntegral $ 4 * VS.length idx) ptr gl_STATIC_DRAW

            let vertices = fromIntegral $ VS.length idx
            let rs' = rs {
                verticesMap = vertices
              , mapData     = mapImage
              }
            return (vertices, rs')
        _ -> return (verticesMap, rs)

    glDrawElements gl_TRIANGLES vertices gl_UNSIGNED_INT nullPtr
    return rs'

renderEntity :: Time -> Entity -> RenderState -> IO ()
renderEntity t entity RenderState{..} =
    case entity of
        EntityPlayer Player{..} Dimension{..} opos npos orot nrot _ -> do
            let Position{..} = interpolate (t / tickFreq) opos npos
                Rotation{..} = interpolate (t / tickFreq) orot nrot
            glUniform2f ufTransform (realToFrac positionX) (realToFrac positionY)
            glUniform2f ufScale (realToFrac $ dimensionWidth / 2) (realToFrac $ dimensionHeight / 2)
            glUniform1f ufRotate $ realToFrac rotationAngle
            glUniform1i ufTex 2
            glUniform4f ufColorMultiplier 1.0 1.0 1.0 (realToFrac playerHealth)

            glBindVertexArray vaoPlayer
            glDrawArrays gl_TRIANGLE_STRIP 0 verticesPlayer
            glUniform4f ufColorMultiplier 1.0 1.0 1.0 1.0
        EntityProjectile Dimension{..} opos npos orot nrot _ -> do
            let Position{..} = interpolate (t / tickFreq) opos npos
                Rotation{..} = interpolate (t / tickFreq) orot nrot
            glUniform2f ufTransform (realToFrac positionX) (realToFrac positionY)
            glUniform2f ufScale (realToFrac $ dimensionWidth / 2) (realToFrac $ dimensionHeight / 2)
            glUniform1f ufRotate $ realToFrac rotationAngle
            glUniform1i ufTex 2

            glBindVertexArray vaoBullet
            glDrawArrays gl_TRIANGLE_STRIP 0 verticesBullet
        _ -> return ()

renderStarfield :: Position -> RenderState -> IO ()
renderStarfield Position{..} RenderState{..} = do
    glUniform2f ufTransform (realToFrac positionX) (realToFrac positionY)
    glUniform2f ufScale     32.0 32.0
    glUniform1f ufRotate    0.0
    glUniform1i ufTex       1

    glBindVertexArray vaoStarfield
    glDrawArrays gl_TRIANGLE_STRIP 0 verticesStarfield
    glClear gl_DEPTH_BUFFER_BIT

renderString :: Dimension -> Position -> String -> RenderState -> IO ()
renderString Dimension{..} Position{..} str RenderState{..} = do
    loadMatrix (orthographicMatrix (realToFrac dimensionWidth) (realToFrac dimensionHeight) 0.0 0.0) ufProjectionMatrix
    glUniform2f ufScale     12.0 27.0
    glUniform1f ufRotate    0.0
    glUniform1i ufTex       3
    glBindVertexArray vaoLetter

    forM_ (zip str [0..]) $ \(c, xoff) ->
        case M.lookup c letterLookup of
            Just c' -> do
                glUniform2f ufTransform (realToFrac positionX + 12 * xoff) (realToFrac positionY)
                glUniform2f ufTexTransform c' 0.0
                glDrawArrays gl_TRIANGLE_STRIP 0 verticesLetter
            Nothing -> error $ "character " ++ show c ++ " not present in font"

    glUniform2f ufTexTransform 0.0 0.0

renderStringCentered :: Dimension -> String -> RenderState -> IO ()
renderStringCentered dim@(Dimension{..}) str rs = do
    forM_ (zip strings [0..]) $ \(line, yoff) ->
        let lenW = (genericLength line - 1) * 6.0 :: Float
            lenH = (genericLength strings) * 27.0 :: Float in
        renderString dim (Position {
            positionX = let a = (dimensionWidth / 2) - lenW in a - a `mod'` 1.0
          , positionY = let a = (dimensionHeight / 2) + lenH / 2 - yoff * 27.0 - 27.0 in a - a `mod'` 1.0
          }) line rs
    where
    strings = lines str

render :: Time -> GameState -> RenderState -> IO RenderState
render t GameState{..} rs@(RenderState{..}) = do
    glViewport 0 0 (floor dimensionWidth) (floor dimensionHeight)
    glClear $ gl_COLOR_BUFFER_BIT .|. gl_DEPTH_BUFFER_BIT .|. gl_STENCIL_BUFFER_BIT

    case gameIsConnected of
        Just msg -> do
            glUniform4f ufColorMultiplier 1.0 1.0 1.0 1.0

            if gameAllowTyping then
                renderStringCentered gameViewport (map toUpper $ "ENTER SERVER ADDRESS:\n\n" ++ gameTypeBuffer ++ "\n") rs
            else
                renderStringCentered gameViewport (map toUpper msg) rs

            return rs
        Nothing -> do
            case getEntityByName gameCurrentPlayer gameMap of
                Just entity -> do
                    let player = playerLastHealthUpdate $ asPlayer entity
                    let cameraPosition = interpolate (t / tickFreq) (entityPositionPrev entity) (entityPosition entity)

                    let t' = min 1 $ ((gameElapsedTime - player)) * 5
                    glUniform4f ufColorMultiplier 1.0 (realToFrac t') (realToFrac t') 1.0

                    renderCamera aspect cameraPosition rs
                    renderStarfield cameraPosition rs

                    rs' <- renderMap gameMap rs

                    glDepthMask $ fromIntegral gl_FALSE
                    mapM_ (\x -> renderEntity t x rs) $ mapEntities gameMap
                    glDepthMask $ fromIntegral gl_TRUE

                    return rs'
                Nothing -> do
                    glUniform4f ufColorMultiplier 1.0 1.0 1.0 1.0
                    renderStringCentered gameViewport "LOADING GAME STATE..." rs
                    return rs
    where
    Dimension{..} = gameViewport
    aspect = dimensionWidth / dimensionHeight
