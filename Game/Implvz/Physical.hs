module Game.Implvz.Physical (
    Physical (..)
  , PhysicalMessage (..)
) where

import Game.Implvz.Types

data Physical =
    PUIRender
  | PMessage !PhysicalMessage
  deriving (Eq, Show)

data PhysicalMessage =
    PKeyboard !Key !KeyState
  | PMotion !Float !Float
  | PReshape !Int !Int
  | PConnected
  | PDisconnected !String
  deriving (Eq, Show)
