module Game.Implvz.Types (
    module Graphics.UI.GLUT
  , Time
  , PlayerName
  , EntityID
  , tickFreq
  , Interpolate (..)
  , compose
  , maybeEither
) where

import qualified Data.ByteString as S
import Graphics.UI.GLUT (Key (..), KeyState (..), SpecialKey (..), MouseButton (..))

type Time = Float
type PlayerName = S.ByteString
type EntityID = Integer

tickFreq :: Time
tickFreq = 1 / 20

class Interpolate a where
    interpolate :: Float -- ^ scalar
                -> a     -- ^ old
                -> a     -- ^ new
                -> a

compose :: a -> [a -> a] -> a
compose a [] = a
compose a (x:xs) = compose (x a) xs

maybeEither :: a -> Maybe b -> Either a b
maybeEither a Nothing  = Left a
maybeEither _ (Just b) = Right b
