module Game.Implvz.Network (
    module Game.Implvz.Protocol
  , NetworkState (..)
  , initialNetworkState
  , isConnected
  , isError
  , connectTCP
  , send
  , recieve
) where

import Control.Applicative
import Control.Concurrent
import Control.Concurrent.STM
import Control.Exception
import Control.Monad
import qualified Data.Attoparsec.ByteString as A
import qualified Data.ByteString as S
import Game.Implvz.Protocol
import qualified Network.Socket as N
import qualified Network.Socket.ByteString as NB
import Prelude hiding (catch)

data NetworkState = NetworkState {
      networkSocket   :: !(TMVar N.Socket)
    , networkSendQ    :: !(TQueue ClientMessage)
    , networkRecieveQ :: !(TQueue ServerMessage)
    , networkSendT    :: !(TMVar ThreadId)
    , networkRecieveT :: !(TMVar ThreadId)
    , networkError    :: !(TMVar String)
    }

initialNetworkState :: IO NetworkState
initialNetworkState = atomically $
    NetworkState <$> newEmptyTMVar
                 <*> newTQueue
                 <*> newTQueue
                 <*> newEmptyTMVar
                 <*> newEmptyTMVar
                 <*> newEmptyTMVar

isConnected :: NetworkState -> IO Bool
isConnected NetworkState{..} = atomically $ not <$> isEmptyTMVar networkSocket

isError :: NetworkState -> IO (Maybe String)
isError NetworkState{..} = atomically $ tryReadTMVar networkError

connectTCP :: N.SockAddr -> NetworkState -> IO ()
connectTCP addr NetworkState{..} = do
    _ <- forkIO $ do
        -- a bug in the network libary on windows causes the runtime to lock-up
        -- while connecting. We sleep a while before connecting to give the
        -- renderer a chance to show the user a friendly "Connecting" message.
        threadDelay 1000000

        sock <- N.socket N.AF_INET N.Stream N.defaultProtocol
        N.setSocketOption sock N.NoDelay 1
        err <- try $ N.connect sock addr :: IO (Either IOException ())

        case err of
            Left e -> atomically . putTMVar networkError $ "UNABLE TO CONNECT TO SERVER\n\n" ++ show e
            Right _ -> do
                tidSend <- forkIO $ (forever $ do
                    msg <- atomically $ readTQueue networkSendQ
                    --putStr ">> " >> print msg
                    NB.send sock $ encodeC msg) `catch` terminateIO

                let loop ir = do
                        d <- NB.recv sock 4096
                        putStr "<< " >> print d
                        enqueue ir d
                    enqueue ir d = let ir' = A.feed ir d in do
                        case ir' of
                            A.Fail _ e e1 -> error $ show e ++ ": " ++ e1
                            A.Partial _   -> loop ir'
                            A.Done r msg  -> do
                                atomically $ writeTQueue networkRecieveQ msg
                                if S.null r then do
                                    loop $ A.parse decodeS ""
                                else do
                                    enqueue (A.parse decodeS "") r

                tidRecieve <- forkIO $ (forever . loop $ A.parse decodeS "") `catch` terminateIO `catch` terminateError

                atomically $ do
                    putTMVar networkSocket sock
                    putTMVar networkSendT tidSend
                    putTMVar networkRecieveT tidRecieve

    return ()
    where
    terminateIO :: IOException -> IO ()
    terminateIO e = atomically $
        putTMVar networkError $ "DISCONNECTED FROM SERVER\n\n" ++ show e

    terminateError :: ErrorCall -> IO ()
    terminateError e =
        atomically $
            putTMVar networkError $ "DISCONNECTED FROM SERVER\n\n" ++ show e

send :: ClientMessage -> NetworkState -> IO ()
send msg NetworkState{..} = atomically $ writeTQueue networkSendQ msg

recieve :: NetworkState -> IO [ServerMessage]
recieve NetworkState{..} = atomically $ loop []
    where
    loop x = do
       q <- tryReadTQueue networkRecieveQ
       case q of
           Just msg -> loop $ x ++ [msg]
           Nothing  -> return x
