module Game.Implvz (
    module Game.Implvz.Physical
  , module Game.Implvz.Types
  , PhysicalState
  , initialState
  , physical
) where

import Control.Monad
import qualified Data.ByteString.Char8 as S8
import qualified Data.Map as M
import Data.Maybe
import qualified Data.Set as Set
import Game.Implvz.Attributes
import Game.Implvz.Game
import Game.Implvz.Network
import Game.Implvz.Objects
import Game.Implvz.Physical
import Game.Implvz.Render
import Game.Implvz.Types
import qualified Network.Socket as N
import System.Random

data PhysicalState = PhysicalState {
    timeRemainder    :: !Float
  , physicalQueue    :: ![PhysicalMessage]
  , gameState        :: !GameState
  , networkState     :: !NetworkState
  , renderState      :: !RenderState
  , networkConnected :: !Bool
  }

initialState :: IO PhysicalState
initialState = do
    ns <- initialNetworkState
    rs <- initialRenderState

    -- random name
    player <- return . S8.pack =<< mapM (const $ randomRIO ('a', 'z')) [1..7 :: Int]

    let p = PhysicalState {
        timeRemainder = 0.0
      , physicalQueue = []
      , gameState = GameState {
            gameElapsedTime = 0.0
          , gameCurrentPlayer = player
          , gameKeysDown = Set.empty
          , gameViewport = Dimension {
                dimensionWidth  = 900
              , dimensionHeight = 900
              }
          , gameMap = Map {
                mapDimension = Dimension {
                    dimensionWidth  = 100
                  , dimensionHeight = 100
                  }
              , mapImage = Nothing
              , mapPlayers = M.empty
              , mapEntities = M.empty
              }
          , gameIsConnected = Just "CONNECTING TO 89.160.187.203:1337..."
          , gameAllowTyping = True
          , gameTypeBuffer  = ""
          }
      , networkState = ns
      , renderState  = rs
      , networkConnected = False
      }

    --address <- N.inet_addr "46.22.105.60"
    --address <- N.inet_addr "89.160.187.203"
    --address <- N.inet_addr "194.144.15.128"
    --connectTCP (N.SockAddrInet 1337 address) ns
    --send (CLogin player) ns

    return p

physical :: Time -> Maybe Physical -> PhysicalState -> IO PhysicalState
physical t p ps@(PhysicalState{..}) = do
    isconn <- isConnected networkState
    iserr  <- isError networkState

    let pm' = compose physicalQueue [
            \x -> case p of
                Just (PMessage p') -> x ++ [p']
                _                  -> x
          , \x -> case iserr of
                Just msg -> x ++ [PDisconnected msg]
                _        -> x
          , \x -> case isconn && isNothing iserr of
                True  -> x ++ [PConnected]
                False -> x
          ]

    (pm, gs, tr) <- loop pm' gameState $ t + timeRemainder

    if not (gameAllowTyping gs) && not networkConnected then do
        address <- N.inet_addr $ gameTypeBuffer gs
        --address <- N.inet_addr "89.160.187.203"
        --address <- N.inet_addr "194.144.15.128"
        connectTCP (N.SockAddrInet 1337 address) networkState
        send (CLogin $ gameCurrentPlayer gs) networkState
    else
        return ()

    rs <- case p of
        Just PUIRender -> do
            render tr gs renderState
        _ -> return renderState

    return ps {
        timeRemainder    = tr
      , physicalQueue    = pm
      , gameState        = gs
      , renderState      = rs
      , networkConnected = networkConnected || not (gameAllowTyping gs)
      }
    where
    loop pm gs t' = do
        case t' >= tickFreq of
            True -> do
                sm <- recieve networkState

                let (gs', cm) = game tickFreq pm sm gs
                forM_ cm $ flip send networkState
                loop [] gs' $ t' - tickFreq
            False -> return (pm, gs, t')
