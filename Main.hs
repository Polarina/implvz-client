module Main (
    main
) where

import Control.Monad
import Data.IORef
import Game.Implvz
import Graphics.UI.GLUT (($=))
import qualified Graphics.UI.GLUT as GLUT
import qualified Network.Socket as N

glutTime :: (Fractional a) => IO a
glutTime = do
    t <- GLUT.get GLUT.elapsedTime
    return $ fromIntegral t / 1000

main :: IO ()
main = N.withSocketsDo $ do
    _ <- GLUT.getArgsAndInitialize
    GLUT.initialDisplayMode $= [GLUT.WithDepthBuffer, GLUT.WithStencilBuffer, GLUT.DoubleBuffered]
    GLUT.initialWindowSize $= GLUT.Size 900 900
    _ <- GLUT.createWindow "Implvz"

    GLUT.globalKeyRepeat $= GLUT.GlobalKeyRepeatOff

    t <- newIORef =<< glutTime
    s <- newIORef =<< initialState

    let tick' = tick t s

    GLUT.displayCallback       $= display tick'
    GLUT.idleCallback          $= Just (idle tick')
    GLUT.keyboardMouseCallback $= Just (keyboard tick')
    GLUT.motionCallback        $= Just (motion tick')
    GLUT.passiveMotionCallback $= Just (motion tick')
    GLUT.reshapeCallback       $= Just (reshape tick')

    forever GLUT.mainLoopEvent

tick :: IORef Float
     -> IORef PhysicalState
     -> Maybe Physical
     -> IO ()
tick t s input = do
    lastTime <- readIORef t
    currTime <- glutTime

    let delta = currTime - lastTime

    s' <- readIORef s
    s'' <- physical delta input s' -- execRWST (physical input) delta s'

    writeIORef s s''
    writeIORef t currTime

display :: (Maybe Physical -> IO ())
        -> GLUT.DisplayCallback
display f = do
    f $ Just PUIRender
    GLUT.swapBuffers
    GLUT.postRedisplay =<< GLUT.get GLUT.currentWindow

idle :: (Maybe Physical -> IO ())
     -> GLUT.IdleCallback
idle f = f Nothing

keyboard :: (Maybe Physical -> IO ())
         -> GLUT.KeyboardMouseCallback
keyboard f key st _ _ = f . Just . PMessage $ PKeyboard key st

motion :: (Maybe Physical -> IO ())
       -> GLUT.MotionCallback
motion f (GLUT.Position x y) = f . Just . PMessage $ PMotion (fromIntegral x) (fromIntegral y)

reshape :: (Maybe Physical -> IO ())
        -> GLUT.ReshapeCallback
reshape f (GLUT.Size w h) = f . Just . PMessage $ PReshape (fromIntegral w) (fromIntegral h)
