#version 120

uniform sampler2D tex;
uniform vec4 colorMultiplier;

varying vec2 out_Texcoord;

vec4 mixe = vec4(1.0, 0.0, 0.0, 1.0);

void main()
{
    gl_FragColor  = texture2D(tex, out_Texcoord);
    gl_FragColor *= colorMultiplier;
}
