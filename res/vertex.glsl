#version 120

uniform mat4  projectionMatrix;
uniform vec2  orthoPosition;
uniform vec2  transform;
uniform vec2  texTransform;
uniform vec2  scale;
uniform float rotate;

attribute vec3 in_Position;
attribute vec2 in_Texcoord;

varying vec2 out_Texcoord;

float c = cos(rotate);
float s = sin(rotate);
mat4 rotation = mat4(
    c, -s,  0,  0,
    s,  c,  0,  0,
    0,  0,  1,  0,
    0,  0,  0,  1
);

void main()
{
    out_Texcoord = in_Texcoord + texTransform;

    gl_Position  = vec4(in_Position.xy * scale, in_Position.z, 1.0);
    gl_Position *= rotation;
    gl_Position += vec4(transform, -31.0, 0.0);
    gl_Position -= vec4(orthoPosition, 0.0, 0.0);
    gl_Position  = projectionMatrix * gl_Position;
}
